//@ Represents a 2d vector
//@
//@ See also: [[file:./matrix.cpp][matrix types]]
template<typename T>
struct Vector2 {
	T x, y;
};
