//////////////////////////////////
//@ * Data Types

//@ Represents a 2x2 matrix, see also: [[file:./vector.cpp][vector types]]
template<typename T>
struct Matrix2x2 {
	//@ Array containing matrix elements in row major order, IE:
	//@ \[ \begin{bmatrix} elements[0] & elements[1] \\ elements[2] & elements[3]\end{bmatrix} \]
	T elements[4];
};

//////////////////////////////////
//@ * Functions

template<typename T>
Vector2<T> transform(Vector2<T> v, Matrix2x2<T> m){
	//@ Computes:
	//@ \[ \begin{bmatrix} v.x \\ v.y \end{bmatrix} \times \begin{bmatrix} m[0][0] & m[0][1] \\ m[1][0] & m[1][1]\end{bmatrix} \]
	//@
	//@ *Parameters*
	//@ | v | The LHS column vector |
	//@ | m | The RHS matrix        |
	return {
		v.x * m.elements[0] + vec.y * mat.elements[2],
		v.x * m.elements[1] + vec.y * mat.elements[3]
	};
}

template<typename T>
Matrix2x2<T> add(Matrix2x2<T> lhs, Matrix2x2<T> rhs){
	return {
		lhs.elements[0] + rhs.elements[0],
		lhs.elements[1] + rhs.elements[1],
		lhs.elements[2] + rhs.elements[2],
		lhs.elements[3] + rhs.elements[3],
 };
}
